class Selector {
    constructor(id, state, style, camera, pos) {
        this.self = $("#" + id);
        if (style === 1) {
            this.self.attr("class", "selector");
        }
        this.style = style;
        this.camera = camera;
        this.pos = pos;

        this.state = state;
        this.setState();
        this.self.click({obj: this}, function (e) {
            e.preventDefault();
            let obj = e.data.obj;
            switch (obj.state) {
                case 0:
                    obj.state = -1;
                    obj.self.css({"background-color" : "#2196f3"});
                    obj.self.text("-");
                    break;
                case -1:
                    obj.state = 1;
                    obj.self.css({"background-color" : "#2196f3"});
                    obj.self.text("+");
                    break;
                case 1:
                    if (style === 1) {
                        obj.state = 0;
                        obj.self.css({"background-color": "white"});
                        obj.self.text(" ");
                    } else {
                        obj.state = -1;
                        obj.self.css({"background-color" : "#2196f3"});
                        obj.self.text("-");
                    }
                    break;
            }
            if(obj.style === 1) {
                let arr = JSON.parse(Cookies.get("array"));
                arr[obj.camera][obj.pos] = obj.state;
                Cookies.set("array", JSON.stringify(arr));
            }
        });
    }

    setState() {
        let obj = this;
        switch (obj.state) {
            case -1:
                obj.self.css({"background-color" : "#2196f3"});
                obj.self.text("-");
                break;
            case 1:
                obj.self.css({"background-color" : "#2196f3"});
                obj.self.text("+");
                break;
            case 0:
                obj.self.css({"background-color" : "white"});
                obj.self.text(" ");
                break;
        }
    }

    setStyle() {

    }
}