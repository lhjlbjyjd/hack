$(document).ready(function(){
    if(getCookie("user_id") === undefined) {
        $("#login-button").click(function () {
            login();
        });
    } else {
        $(location).attr('href', "http://localhost:63342/Hack/index.html")
    }
});

function login() {
    let login = $("#login").val();
    let pass = $("#password").val();
    if(login === "") {
        return;
    }
    if(pass === "") {
        return;
    }
    if(login === "user" && pass === "pass") {
        document.cookie = "user_id=0; path=/; expires=" + new Date(new Date().getTime() + 24 * 60 * 60 * 1000).toUTCString();
        $(location).attr('href', "http://localhost:63342/Hack/index.html")
    }
}

function getCookie(name) {
    let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}