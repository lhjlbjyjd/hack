let current_camera = 1;
let blink_interval;

$(document).ready(function(){
    $(".grid-container").css({"height": $( window ).height()});
    $("#side-panel").css({"height": $( window ).height(), "max-height" : $( window ).height()})

    let current_selection = undefined;

    for( i = 1; i < 5; i++) {
        $("#viewport-" + i).parent().attr({"data-camera" : i});
        $("#viewport-" + i).parent().click(function (e) {
            let side = $("#side-panel");
            let target = $(e.target);
            let current = target.parent();
            let p = current.css("background-color");
            setSideData(target);
            if (current_selection !== undefined) {
                current_selection.parent().css({"background-color": "#FFFFFF"});
                if (target.is(current_selection)) {
                    current_selection = undefined;
                    current_camera = 1;
                    side.animate({
                        width: "0"
                    }, 500, function () {
                        side.css({"visibility" : "hidden"});
                    });
                } else {
                    current_selection = target;
                    current_camera = target.attr("data-camera");
                    target.parent().css({"background-color": "#009bff"});
                }
            } else {
                side.animate({
                    width: "30%"
                }, 500);
                side.css({"visibility" : "visible"});
                current.css({"background-color": "#009bff"});
                current_selection = target;
                current_camera = target.attr("data-camera");
            }
            refreshData();
        });
        let place = new VideoAdapter("viewport-"+ i, "hack-change/rubbish/output" + i + ".jpg");
        setInterval(updateVideo, 500, place);
        blink_interval = setInterval(signal, 300);
    }

    if(Cookies.get("array") === undefined) {
        Cookies.set("array", "[[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0],[0,0,0,0,0,0,0,0]]");
    }

    saveData();

});

function refreshData() {
    $("#main-list").empty();
    $.getJSON("json/modules.json", function(json) {
        for (i in json) {
            $("#main-list").append("<div class=\"situation-checker\" data-custom='false'><span id=\"main-selector-" + i + "\"></span>" + json[i].name + "</div>");
            new Selector("main-selector-" + i, JSON.parse(Cookies.get("array"))[current_camera - 1][i], 1, current_camera - 1, i);
        }
    });
    let arr = JSON.parse(Cookies.get("custom"));
    if(arr !== "" && arr !== undefined) {
        for(i = 0; i < arr.length; i++) {
            $("#main-list").append("<div class=\"situation-checker\" data-custom='true'><span id=\"main-selector-" + 10 + i + "\"></span>" + arr[i].name + "</div>");
            new Selector("main-selector-" + 10 + i, 0, 1, current_camera - 1, 10 + i);
        }
    }
}

$(window).resize(function(){
    $(".grid-container").css({"height": $( window ).height(), "max-height" : $( window ).height()});
    $("#side-panel").css({"height": $( window ).height(), "max-height" : $( window ).height()})
});

function setSideData(i) {
    $("#stream-name").text(i.attr('data-name'));
}

function updateVideo(where) {
    where.update();
}

function kostyl(id) {
    let current = $("#"+ id);
    current.parent().css({"background-image": current.css("background-image")});
}

function allowDrop(ev) {
    ev.preventDefault();
}

function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

function drop(ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    if(ev.target.id === "add-creator") {
        ev.target.appendChild(document.getElementById(data));
        $("#" + "add-checker-" + data[data.length - 1]).addClass("selector");
        $("#" + data).dblclick(function (e) {
            if(e.target.nodeName === "DIV") {
                let curr = $(e.target);
                $("#" + "add-checker-" + e.target.id[e.target.id.length - 1]).removeClass("selector");
                $("#" + "add-checker-" + e.target.id[e.target.id.length - 1]).text(" ");
                $("#add-list").append(curr);
            }
        });
        $("#" + "add-checker-" + data[data.length - 1]).trigger("click");
        console.log(ev.dataTransfer.getData("lol"));
    }
}

function showModal() {
    $("#modal").css({"visibility" : "visible"});
    $("#test").css({"visibility" : "visible", "background-color" : "rgb(0, 0, 0, 0.7)"});
    $.getJSON("json/modules.json", function(json) {
        $("#add-list").empty();
        $("#add-creator").empty();
        for (i in json) {
            let curr = $("<div class=\"situation-checker\" id='add-checker-layout-" + i + "' data-id=\""+ i +"\" ondragstart=\"drag(event)\" draggable=\"true\"><span id=\"add-checker-" + i + "\"></span>" + json[i].name + "</div>");
            $("#add-list").append(curr);
            new Selector("add-checker-" + i, -1, 0, 0, 0);
            $("#add-checker-" + i).text(" ");
        }
    });
    let arr = JSON.parse(Cookies.get("custom"));
    if(arr !== "" && arr !== undefined) {
        for(i = 0; i < arr.length; i++) {
            let curr = $("<div class=\"situation-checker\" id='add-checker-layout-" + 10 + i + "' data-id=\""+ 10 + i +"\" ondragstart=\"drag(event)\" draggable=\"true\"><span id=\"add-checker-" + 10 + i + "\"></span>" + arr[i].name + "</div>");
            $("#add-list").append(curr);
            new Selector("add-checker-" + 10 + i, -1, 0, 0, 0);
            $("#add-checker-" + 10 + i).text(" ");
        }
    }
}

function signal() {
    for(i = 1; i < 5; i++) {
        tmp(i);
    }
}

function tmp(i) {
    $.getJSON("hack-change/rubbish/answer"+ i +".json", function (json) {
        for(j = 0; j < json.length; j++) {
            if ((json[j] === 1 && JSON.parse(Cookies.get("array"))[i - 1][j] === 1) || (json[j] === 0 && JSON.parse(Cookies.get("array"))[i - 1][j] === -1)) {
                let curr = $("#grid-item-" + i);
                if(curr.css("background-color") === "rgb(255, 255, 255)") {
                    curr.css({"background-color" : "red"});
                }
                return;
            }
        }
    });
}

function hideModal() {
    $("#modal").css({"visibility" : "hidden"});
    $("#test").css({"visibility" : "hidden", "background-color" : "rgb(0, 0, 0, 0.7)"})
}

function savePreset() {
    let u = $("#add-creator");
    let name = $("#add-name").val();
    if(name !== "" && u.children().length > 1) {
        let arr = Cookies.get("custom");
        if (arr === undefined || arr === "") {
            arr = "[]";
        }
        arr = JSON.parse(arr);
        let tst = [];
        $(i).attr("data-id");
        let children = $("#add-creator").children();
        for(i = 0; i < children.length; i++) {
            tst.push({"id" : $(children[i]).attr("data-id"), "value": ($($(children[i]).children()[0]).text() === "+")});
        }
        arr.push({"name" : name, "used" : tst});
        Cookies.set("custom", arr);
        refreshData();
        hideModal();
    }
}

function saveData() {
    if($("#add-name").val() !== "") {

    }
    refreshData();
}

class VideoAdapter {

    constructor(id, image) {
        this.place_id = id;
        this.image = image;
    }

    update() {
        let current = $("#"+ this.place_id);
        current.css({"visibility" : "hidden", "background-image": "url(" + this.image + "?" + new Date().getTime() + ")"});
        setTimeout(kostyl, 255, this.place_id);
    }

}